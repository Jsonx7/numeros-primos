let html = document.getElementById('show');
let contador = 0;

function numeroprimos(max) {
    for (let i = 2; i < max; i++) {
        if (contador < 10) {
            if (esPrimo(i)) {
                html.innerHTML += `${i} <br/>`;
                contador++;
            }
        } else {
            break;
        }

    }
}

function esPrimo(n) {
    for (let i = 2; i < n; i++) {
        if (n % i === 0) {
            return false;
        }
    }
    return true;
}
numeroprimos(100);